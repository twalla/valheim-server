# Valheim Dedicated Server

## Setup

`TODO`

## Importing or creating a world

IMPORTANT: If you rename your world after creating it, it will NOT WORK. The name and byte length of the name are hardcoded into the `.fwl` file. You can change this with a hex editor by replacing the name and changing the byte before the first char of the name to the length of the new name.


If you pass the start command the name of a world that does not exist, it will create a new one of that name from a random seed. If you want to use a specific seed you need to create the world in game and import it. Your local world files can be found here at the following locations:

### Windows

`C:\Users\%username%\AppData\LocalLow\IronGate\Valheim\worlds`

### Linux

`~/.config/unity3d/IronGate/Valheim/worlds/`

## Connecting

- From your Steam client naviate to `View` > `Servers`
- Select `Add Server`
- Enter your server IP and port, the listening port is the game port +1, so if the server is running at 1.2.3.4 on port 2456 then enter `1.2.3.4:2457`
- Selecting 'Find Games at this Address` should show a game Valheim game with the server name you selected earlier running.
- Select `Add this Address to Favorites`
- After adding to favorites you should be able to double click and join the game after entering the password.

`TODO: Add pictures`

## Resources

`TODO`
