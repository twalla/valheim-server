#!/bin/bash

# we're operating under the assumption the image is apt-based 
# and already has docker and docker-compose

# update/upgrade
export DEBIAN_FRONTEND=noninteractive

apt-get -qy update
apt-get -qy upgrade

# pull down the project
git clone https://gitlab.com/twalla/valheim-server.git /srv/valheim-server

# inject config
# TODO: for now just do this manually after cloning

# create a folder for game data
mkdir -p /srv/valheim/worlds
chown -R 1000:1000 /srv/valheim/

# create systemd unitfile
cat <<EOT >> /etc/systemd/system/valheim-server.service
# /etc/systemd/system/valheim-server.service

[Unit]
Description=Valheim Server
Requires=docker.service
After=docker.service

[Service]
Type=oneshot
RemainAfterExit=yes
WorkingDirectory=/srv/valheim-server
ExecStart=/usr/local/bin/docker-compose up -d
ExecStop=/usr/local/bin/docker-compose down
TimeoutStartSec=0

[Install]
WantedBy=multi-user.target
EOT

systemctl enable valheim-server
